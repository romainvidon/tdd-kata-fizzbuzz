package info.romainvidon.kata;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FizzBuzzTest {

    @Test
    @DisplayName("Should return the number as a String")
    public void fizzBuzzShouldReturnSameNumber() {
        assertEquals("43", FizzBuzz.fizzBuzz(43));
    }

    @ParameterizedTest
    @ValueSource(ints = { 3, 6, 9, 27, 42, 636939333 })
    @DisplayName("Should return \"Fizz\" if the number is divisible by 3")
    public void multipleOf3ShouldReturnFizz(int number) {
        assertEquals("Fizz", FizzBuzz.fizzBuzz(number));
    }

    @ParameterizedTest
    @ValueSource(ints = { 25, 40 })
    @DisplayName("Should return \"Buzz\" if the number is divisible by 5")
    public void multipleOf5ShouldReturnBuzz(int number) {
        assertEquals("Buzz", FizzBuzz.fizzBuzz(number));
    }

    @ParameterizedTest
    @ValueSource(ints = { 15, 30 })
    @DisplayName("Should return \"FizzBuzz\" if the number is divisible by 3 and by 5")
    public void multipleOf3and5ShouldReturnFizzBuzz(int number) {
        assertEquals("FizzBuzz", FizzBuzz.fizzBuzz(number));
    }

}
